package com.cardinal.wings.fragments.about;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    public static final String PREFS_NAME = "About DU";

    final ThreadLocal<SlidingTabLayout> mSlidingTabLayout = new ThreadLocal<>();
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.initLayout();
        this.addContent();
    }

    public void initLayout() {
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setOffscreenPageLimit(10);
        mViewPager.setAdapter(new SlidingTabAdapter());

        mSlidingTabLayout.set((SlidingTabLayout) findViewById(R.id.sliding_tabs));
        mSlidingTabLayout.get().setCustomTabView(R.layout.toolbar_tab, R.id.toolbar_tab_txtCaption);
        mSlidingTabLayout.get().setSelectedIndicatorColors(getResources().getColor(R.color.tab_indicator_color));
        mSlidingTabLayout.get().setViewPager(mViewPager);
    }

    public void addTab(int layout,String tabTitle)
    {
        this.addTab(layout, tabTitle, -1);
    }
    public void addTab(int layout,String tabTitle,int position)
    {
        SlidingTabAdapter mTabs = (SlidingTabAdapter)mViewPager.getAdapter();
        mTabs.addView(getLayoutInflater().inflate(layout, null), tabTitle, position);
        mTabs.notifyDataSetChanged();
        mSlidingTabLayout.get().populateTabStrip();
    }

    public void addContent(){
        addTab(R.layout.about_du, getString(R.string.general_info));
        addTab(R.layout.developers, getString(R.string.developers));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menuitem_exit:

                SharedPreferences prefs = this.getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor prefEditor = prefs.edit();
                prefEditor.commit();
                finish();
                return true;
        }
        return false;
    }
    public void launchAlex(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.alex_url)));
        startActivity(intent);
    }
    public void launchAnders(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.anders_url)));
        startActivity(intent);
    }
    public void launchBosko(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.bosko_url)));
        startActivity(intent);
    }
    public void launchBret(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.bret_url)));
        startActivity(intent);
    }
    public void launchChris(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.chris_url)));
        startActivity(intent);
    }
    public void launchDaniel(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.daniel_url)));
        startActivity(intent);
    }
    public void launchDorian(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.dorian_url)));
        startActivity(intent);
    }
    public void launchEdwin(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.edwin_url)));
        startActivity(intent);
    }
    public void launchGlen(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.glen_url)));
        startActivity(intent);
    }
    public void launchJames(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.james_url)));
        startActivity(intent);
    }
    public void launchJosh1(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.josh1_url)));
        startActivity(intent);
    }
    public void launchJosh2(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.josh2_url)));
        startActivity(intent);
    }
    public void launchJosh3(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.josh3_url)));
        startActivity(intent);
    }
    public void launchJosip(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.josip_url)));
        startActivity(intent);
    }
    public void launchMark(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.mark_url)));
        startActivity(intent);
    }
    public void launchMichael(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.michael_url)));
        startActivity(intent);
    }
    public void launchNick(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.nick_url)));
        startActivity(intent);
    }
    public void launchNico(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.nico_url)));
        startActivity(intent);
    }
    public void launchRandall(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.randall_url)));
        startActivity(intent);
    }
    public void launchSean(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.sean_url)));
        startActivity(intent);
    }
    public void launchSusan(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.susan_url)));
        startActivity(intent);
    }
    public void launchTony(View view) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(getString(R.string.tony_url)));
        startActivity(intent);
    }
}
